<?php

/**
 * Contrôleur : gestion des groupes
 */
use modele\metier\Groupe;
use modele\dao\GroupeDAO;
use modele\dao\Bdd;
require_once __DIR__ . '/includes/autoload.php';
Bdd::connecter();

include("includes/_gestionErreurs.inc.php");

// 1ère étape (donc pas d'action choisie) : affichage de l'ensemble des groupes
if (!isset($_REQUEST['action'])) {
    $_REQUEST['action'] = 'initial';
}

$action = $_REQUEST['action'];

// Aiguillage selon l'étape
switch ($action) {
    case 'initial':
        include("vues/GestionGroupes/vObtenirGroupes.php");
        break;

    case 'demanderSupprimerGroupe':
        $id = $_REQUEST['id'];
        include("vues/GestionGroupes/vSupprimerGroupe.php");
        break;

    case 'demanderCreerGroupe':
        include("vues/GestionGroupes/vCreerModifierGroupe.php");
        break;

    case 'demanderModifierGroupe':
        $id = $_REQUEST['id'];
        include("vues/GestionGroupes/vCreerModifierGroupe.php");
        break;

    case 'validerSupprimerGroupe':
        $id = $_REQUEST['id'];
        GroupeDAO::delete($id);
        include("vues/GestionGroupes/vObtenirGroupes.php");
        break;

    case 'validerCreerGroupe':
        $id = $_REQUEST['id'];
        $nom = $_REQUEST['nom'];
        $idResp = $_REQUEST['idResp'];
        $adPost = $_REQUEST['adPost'];
        $nbPers = $_REQUEST['nbPers'];
        $nomPays = $_REQUEST['nomPays'];
        $heberg = $_REQUEST['heberg'];
        verifierDonneesGroupeC($id, $nom, $nbPers, $nomPays, $heberg);
        if (nbErreurs() == 0) {
            GroupeDAO::insert(new Groupe($id, $nom, $idResp, $adPost, $nbPers, $nomPays, $heberg));
            include("vues/GestionGroupes/vObtenirGroupes.php");
        } else {
            include("vues/GestionGroupes/vCreerModifierGroupe.php");
        }
        break;

    case 'validerModifierGroupe':
        $id = $_REQUEST['id'];
        $nom = $_REQUEST['nom'];
        $idResp = $_REQUEST['idResp'];
        $adPost = $_REQUEST['adPost'];
        $nbPers = $_REQUEST['nbPers'];
        $nomPays = $_REQUEST['nomPays'];
        $heberg = $_REQUEST['heberg'];
        verifierDonneesGroupeM($id, $nom, $nbPers, $nomPays, $heberg);
        if (nbErreurs() == 0) {
            GroupeDAO::update($id, new Groupe($id, $nom, $idResp, $adPost, $nbPers, $nomPays, $heberg));
            include("vues/GestionGroupes/vObtenirGroupes.php");
        } else {
            include("vues/GestionGroupes/vCreerModifierGroupe.php");
        }
        break;
}

// Fermeture de la connexion au serveur MySql
Bdd::deconnecter();

function verifierDonneesGroupeC($id, $nom, $nbPers, $nomPays, $heberg) {
    if ($id == "" || $nom == "" || $nbPers == "" || $nomPays == "" || $heberg == "") {
        ajouterErreur('Chaque champ suivi du caractère * est obligatoire');
    }
    if ($id != "") {
        // Si l'id est constitué d'autres caractères que de lettres non accentuées 
        // et de chiffres, une erreur est générée
        if (!estChiffresOuEtLettres($id)) {
            ajouterErreur("L'identifiant doit comporter uniquement des lettres non accentuées et des chiffres");
        } else {
            if (GroupeDAO::isAnExistingId($id)) {
                ajouterErreur("Le type de groupe $id existe déjà");
            }
        }
    }
    if ($nom != "" && GroupeDAO::isAnExistingNom(true, $id, $nom)) {
        ajouterErreur("Le nom de groupe $nom existe déjà");
    }
}

function verifierDonneesTypeChambreM($id, $libelle) {
    if ($libelle == "") {
        ajouterErreur('Chaque champ suivi du caractère * est obligatoire');
    }
    if ($libelle != "" && TypeChambreDAO::isAnExistingLibelle(false, $id, $libelle)) {
        ajouterErreur("Le nom de groupe $nom existe déjà");
    }
}
