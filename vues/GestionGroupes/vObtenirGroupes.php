<?php
use modele\dao\GroupeDAO;
use modele\dao\AttributionDao;
use modele\dao\Bdd;
require_once __DIR__ . '/../../includes/autoload.php';

include("includes/_debut.inc.php");

// AFFICHER L'ENSEMBLE DES GROUPES 
// CETTE PAGE CONTIENT UN TABLEAU CONSTITUÉ D'1 LIGNE D'EN-TÊTE ET D'1 LIGNE PAR 
// GROUPE

echo "
<br>
<table width='40%' cellspacing='0' cellpadding='0' class='tabNonQuadrille'>
   <tr class='enTeteTabNonQuad'>
      <td colspan='4'><strong>Groupes</strong></td>
   </tr>";
$lesGroupes = GroupeDAO::getAll();


// BOUCLE SUR LES TYPES DE CHAMBRES
foreach ($lesGroupes as $unGroupe) {
    $id = $unGroupe->getId();
    $nom = $unGroupe->getNom();
    $idResp = $unGroupe->getIdentite();
    $adresse = $unGroupe->getAdresse();
    $nbPers = $unGroupe->getNbPers();
    $nomPays = $unGroupe->getNomPays();
    $heberg = $unGroupe->getHebergement();
    echo "
      <tr class='ligneTabNonQuad'> 
         <td width='15%'>$id</td>
         <td width='33%'>$nom</td>
         <td width='26%' align='center'>
         
         <a href='cGestionGroupes.php?action=demanderModifierGroupe&id=$id'>
         <button type=button>Modifier</button></a></td>";

    // S'il existe déjà des attributions pour le groupe, il faudra
    // d'abord les supprimer avant de pouvoir supprimer le groupe
    if (!AttributionDao::existeAttributionsGroupe($id)) {
        echo "
            <td width='26%' align='center'>
            <a href='cGestionGroupes.php?action=demanderSupprimerGroupe&id=$id'>
            <button type=button>Supprimer</button></a></td>";
    } else {
        echo "<td width='26%'>&nbsp; </td>";
    }
    echo "               
    </tr>";
}
echo "    
</table><br>
<a href='cGestionGroupes.php?action=demanderCreerGroupe'>
<button type=button>Création d'un groupe</button></a>";

include("includes/_fin.inc.php");

