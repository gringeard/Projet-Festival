<?php

namespace modele\metier;

/**
 * Description of Attribution
 * @author btssio
 */
class Attribution {

    /**
     * objet de type Offre
     * @var Offre
     */
    private $offre;

    /**
     * objet de type Groupe
     * @var Groupe
     */
    private $groupe;

    /**
     * nombre de chambres pour un type de chambre dans un établissement
     * @var string
     */
    private $nombreChambres;
    function __construct(Offre $offre, Groupe $groupe, $nombreChambres) {
        $this->offre = $offre;
        $this->groupe = $groupe;
        $this->nombreChambres = $nombreChambres;
    }
    function getOffre() {
        return $this->offre;
    }

    function getGroupe() {
        return $this->groupe;
    }

    function getNombreChambres() {
        return $this->nombreChambres;
    }

    function setOffre(Offre $offre) {
        $this->offre = $offre;
    }

    function setGroupe(Groupe $groupe) {
        $this->groupe = $groupe;
    }

    function setNombreChambres($nombreChambres) {
        $this->nombreChambres = $nombreChambres;
    }


   
}
