<?php
namespace modele\metier;

/**
 * Description of Offre
 * @author btssio
 */
class Offre {
    /**
     * objet de type d'Etablissement
     * @var Etablissement
     */
    private $etab;
    /**
     * objet de type TypeChambre
     * @var TypeChambre
     */
    private $typeChambre;
    /**
     * nombre de chambres pour un type de chambre dans un établissement
     * @var string
     */
    private $nombreChambres;
       
    function __construct(Etablissement $etab, TypeChambre $typeChambre, $nombreChambres) {
        $this->etab = $etab;
        $this->typeChambre = $typeChambre;
        $this->nombreChambres = $nombreChambres;
    }

    function getEtab() {
        return $this->etab;
    }

    function getTypeChambre() {
        return $this->typeChambre;
    }

    function getNombreChambres() {
        return $this->nombreChambres;
    }

    function setEtab(Etablissement $etab) {
        $this->etab = $etab;
    }

    function setTypeChambre(TypeChambre $typeChambre) {
        $this->typeChambre = $typeChambre;
    }

    function setNombreChambres($nombreChambres) {
        $this->nombreChambres = $nombreChambres;
    }


}
